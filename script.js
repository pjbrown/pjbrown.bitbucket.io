(function()
{
	var storedScrollTop = 0;
	var initialScrollTop = 200;

	function headerScroll(ev)
	{
		var scrollTop = $(this).scrollTop();

		if (scrollTop < storedScrollTop)
		{
			$(".header-fixed").removeClass("header-fixed-rise").addClass("header-fixed-drop");
		}
		else if (scrollTop > initialScrollTop)
		{
			$(".header-fixed").removeClass("header-fixed-drop").addClass("header-fixed-rise");
		}

		storedScrollTop = scrollTop;
	}
	$(window).bind("touchmove", headerScroll);
	$(window).scroll(headerScroll);

})();